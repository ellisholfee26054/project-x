package com.dmytro.service.serviceImpl;

import com.dmytro.domain.Customer;
import com.dmytro.mapper.CustomerMapper;
import com.dmytro.model.CustomerDto;
import com.dmytro.repository.CustomerRepository;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class})
class CustomerServiceImplTest implements WithAssertions {

    @Mock
    private CustomerMapper customerMapper;
    @Mock
    private CustomerRepository customerRepository;
    @InjectMocks
    private CustomerServiceImpl customerService;

    @Test
    void findAllCustomers() {
        //arrange
        //act
        //assert
        final Customer customer1 = new Customer().setId(1);
        final Customer customer2 = new Customer().setId(2);

        when(customerRepository.findAll())
                .thenReturn(List.of(
                        customer1,
                        customer2));

        final CustomerDto customerDto1 = customerMapper.toDto(customer1);
        final CustomerDto customerDto2 = customerMapper.toDto(customer2);

        when(customerMapper.toDto(customer1)).thenReturn(customerDto1);
        when(customerMapper.toDto(customer2)).thenReturn(customerDto2);

        final List<CustomerDto> result = customerService.findAllCustomers();

        assertThat(result).hasSize(2);
        assertThat(result).contains(customerDto1, customerDto2);

        verify(customerRepository).findAll();
    }

    @Test
    void findCustomer() {
        final Integer customerId = 1;
        Customer customer = new Customer().setId(customerId);
        CustomerDto customerDto = new CustomerDto().setId(customerId);

        when(customerRepository.findById(customerId)).thenReturn(Optional.of(customer));
        when(customerMapper.toDto(customer)).thenReturn(customerDto);

        CustomerDto result = customerService.findCustomer(customerId);

        assertThat(result).isEqualTo(customerDto);

        verify(customerMapper).toDto(customer);
        verify(customerRepository).findById(customerId);
    }
}