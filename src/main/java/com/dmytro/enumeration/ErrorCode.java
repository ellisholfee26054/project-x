package com.dmytro.enumeration;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ErrorCode {
    GENERIC_ERROR("0000"),
    PRODUCT_NOT_FOUND("0001"),
    ARGUMENTS_NOT_VALID("0002"),
    AUTHOR_NOT_FOUND("0003"),
    ORDER_NOT_FOUND("0004");

    private final String code;
}
