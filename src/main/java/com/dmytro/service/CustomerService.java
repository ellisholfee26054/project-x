package com.dmytro.service;

import com.dmytro.model.CustomerRequestDto;
import com.dmytro.model.CustomerDto;

import java.util.List;

public interface CustomerService {

    List<CustomerDto> findAllCustomers();

    CustomerDto findCustomer(Integer id);

    void deleteById(Integer id);

    CustomerDto addCustomer(CustomerRequestDto request);

    CustomerDto updateCustomer(Integer id, CustomerRequestDto request);
}
