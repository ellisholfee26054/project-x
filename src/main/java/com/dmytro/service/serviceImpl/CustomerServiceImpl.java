package com.dmytro.service.serviceImpl;

import com.dmytro.exception.CustomerNotFoundException;
import com.dmytro.repository.CustomerRepository;
import com.dmytro.model.CustomerRequestDto;
import com.dmytro.domain.Customer;
import com.dmytro.mapper.CustomerMapper;
import com.dmytro.model.CustomerDto;
import com.dmytro.service.CustomerService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    private final CustomerMapper customerMapper;

    @Override
    @Transactional
    public List<CustomerDto> findAllCustomers() {
        return customerRepository.findAll()
                .stream()
                .map(customerMapper::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public CustomerDto findCustomer(Integer id) {
        Customer result = customerRepository.findById(id)
                .orElseThrow(() -> new CustomerNotFoundException(id));

        return customerMapper.toDto(result);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        log.info("Deleting customer by id " + id);
        customerRepository.deleteById(id);
    }

    @Override
    @Transactional
    public CustomerDto addCustomer(CustomerRequestDto request) {
        log.info("Adding and saving customer to database");

        final Customer customer = customerMapper.toDomain(request);
        final Customer savedCustomer = customerRepository.save(customer);

        log.debug("Customer with successfully saved");
        return customerMapper.toDto(savedCustomer);
    }

    @Override
    @Transactional
    public CustomerDto updateCustomer(Integer id, CustomerRequestDto request) {
        log.info("Updating product with id " + id);

        Customer customer = customerRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        customerMapper.mergeCustomer(customer, request);

        CustomerDto result = customerMapper.toDto(customer);

        log.debug("Returning " + result.toString());
        return result;
    }
}
