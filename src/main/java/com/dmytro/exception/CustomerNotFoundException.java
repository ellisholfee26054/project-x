package com.dmytro.exception;

import com.dmytro.enumeration.ErrorCode;
import org.springframework.http.HttpStatus;

public class CustomerNotFoundException extends AppException {

    public CustomerNotFoundException(Integer id) {
        super("Customer with id " + id + " not found!", ErrorCode.PRODUCT_NOT_FOUND, HttpStatus.NOT_FOUND);
    }
}
