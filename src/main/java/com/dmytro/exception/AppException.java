package com.dmytro.exception;

import com.dmytro.enumeration.ErrorCode;
import lombok.Getter;
import org.springframework.http.HttpStatus;


@Getter
public abstract class AppException extends RuntimeException {

    private final ErrorCode errorCode;

    private final HttpStatus httpStatus;

    public AppException(String message, ErrorCode errorCode, HttpStatus httpStatus) {
        super(message);
        this.errorCode = errorCode;
        this.httpStatus = httpStatus;
    }
}
