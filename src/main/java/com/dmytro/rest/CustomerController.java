package com.dmytro.rest;

import com.dmytro.model.CustomerRequestDto;
import com.dmytro.service.CustomerService;
import com.dmytro.model.CustomerDto;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/customers")
@RequiredArgsConstructor
public class CustomerController {
    private final CustomerService customerService;

    @GetMapping
    public List<CustomerDto> getCustomers() {
        return customerService.findAllCustomers();
    }

    @GetMapping("{id}")
    public CustomerDto findCustomer(@PathVariable("id") Integer id) {
        return customerService.findCustomer(id);
    }

    @PostMapping
    public CustomerDto addCustomer(@Valid @RequestBody CustomerRequestDto request) {
        return customerService.addCustomer(request);
    }

    @PutMapping("{id}")
    CustomerDto updateCustomer(@PathVariable("id") Integer id, @Valid @RequestBody CustomerRequestDto request) {
        return customerService.updateCustomer(id, request);
    }

    @DeleteMapping("{id}")
    void deleteCustomer(@PathVariable("id") Integer id) {
        customerService.deleteById(id);
    }

}
