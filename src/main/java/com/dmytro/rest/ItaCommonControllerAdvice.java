package com.dmytro.rest;

import com.dmytro.enumeration.ErrorCode;
import com.dmytro.exception.AppException;
import com.dmytro.model.ExceptionDto;
import jakarta.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.stream.Collectors;

@Component
@ControllerAdvice
@Slf4j
public class ItaCommonControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(AppException.class)
    public ResponseEntity<Object> handleItaException(
            AppException ex,
            ServletWebRequest request
    ) {
        log.error("An exception occurred while processing " +  request.getRequest().getMethod() + " at " + request.getRequest().getRequestURL(), ex);

        return handleExceptionInternal(
                ex,
                new ExceptionDto(
                        ex.getMessage(),
                        ex.getErrorCode().getCode()
                ),
                new HttpHeaders(),
                ex.getHttpStatus(),
                request
        );
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleUnexpectedException(
            Exception ex,
            ServletWebRequest request
    ) {
        log.error("An exception occurred while processing " +  request.getRequest().getMethod() + " at " + request.getRequest().getRequestURL(), ex);

        return handleExceptionInternal(
                ex,
                new ExceptionDto(
                        ex.getMessage(),
                        ErrorCode.GENERIC_ERROR.getCode()
                ),
                new HttpHeaders(),
                HttpStatus.INTERNAL_SERVER_ERROR,
                request
        );
    }

    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            @NotNull MethodArgumentNotValidException exception,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        ServletWebRequest servletWebRequest = (ServletWebRequest) request;
        log.error("An exception occurred while processing " +  servletWebRequest.getRequest().getMethod() + " at " + servletWebRequest.getRequest().getRequestURL(), exception);


        final String errors = exception.getBindingResult().getAllErrors().stream()
                .map(error -> ((FieldError) error).getField() + " " + error.getDefaultMessage())
                .collect(Collectors.joining(", "));

        return handleExceptionInternal(
                exception,
                new ExceptionDto(
                        errors,
                        ErrorCode.ARGUMENTS_NOT_VALID.getCode()
                ),
                new HttpHeaders(),
                status,
                request
        );
    }
}
