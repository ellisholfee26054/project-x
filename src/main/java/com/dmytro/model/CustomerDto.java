package com.dmytro.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CustomerDto {
    private Integer id;
    private String name;
    private String email;
    private Integer age;
}
