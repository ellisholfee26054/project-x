package com.dmytro.model;



public record ExceptionDto(String message, String code) {
}
