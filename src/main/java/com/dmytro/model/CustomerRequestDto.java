package com.dmytro.model;


public record CustomerRequestDto (String name, String email, Integer age){

}
