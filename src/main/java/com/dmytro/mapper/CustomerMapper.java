package com.dmytro.mapper;

import com.dmytro.model.CustomerRequestDto;
import com.dmytro.domain.Customer;
import com.dmytro.model.CustomerDto;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface CustomerMapper {

    CustomerDto toDto(Customer customer);

    Customer toDomain(CustomerRequestDto customerDto);

    void mergeCustomer(@MappingTarget Customer target, CustomerRequestDto source);

}
